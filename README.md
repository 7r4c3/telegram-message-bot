# Telegram Message Bot
Simple bot class to send messages into telegram chats/groups. 
## Setup
- [Create](https://core.telegram.org/bots/tutorial#obtain-your-bot-token) a new bot to receive access token
- To receive a chat id, contact the bot in chat or add it to a group and call the method get_latest_chat_id()
- Thats it. Now you can send messages to the chat or group via send_message_get() and send_message_post()