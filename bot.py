import requests


class Bot:
    def __init__(self, token, chat_id):
        self.token = token
        self.chat_id = chat_id

    def set_token(self, token):
        self.token = token

    def set_chat_id(self, chat_id):
        self.chat_id = chat_id

    def get_updates(self):
        url = "https://api.telegram.org/bot{}/getUpdates".format(self.token)
        response = requests.get(url).json()
        return response['result']

    def get_latest_chat_id(self):
        updates = self.get_updates()
        return updates[-1]['message']['chat']['id']

    def send_message_get(self, msg):
        if self.token and self.chat_id:
            response = requests.get(
                'https://api.telegram.org/bot{token}/sendMessage?chat_id={chat_id}&parse_mode=Markdown&text={msg}'.format(
                    token=self.token,
                    chat_id=self.chat_id,
                    msg=msg
                )
            )
        else:
            raise Exception("Please set Token/Chatid")

    def send_message_post(self, msg):
        if self.token and self.chat_id:
            response = requests.post(
                url="https://api.telegram.org/bot{0}/sendMessage".format(self.token),
                data={"chat_id": self.chat_id,
                      "parse_mode": "HTML",
                      "text": msg
                      }
            ).json()
        else:
            raise Exception("Please set Token/Chatid")

    def recv_command(self):
        pass
